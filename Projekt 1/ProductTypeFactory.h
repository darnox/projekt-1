#pragma once
#include "Products.h"
typedef ProductType::ProductTypes pt;
class ProductTypeFactory{
public:
	static ProductType *createProductType(const pt& type, int quantity) {
		if (type == pt::Milk) return new Milk(quantity);
		else if(type == pt::Bread) return new Bread(quantity);
		else if (type == pt::Apples) return new Apples(quantity);
		else if (type == pt::Chicken) return new Chicken(quantity);
		else if (type == pt::Eggs) return new Eggs(quantity);
		else if (type == pt::Water) return new Water(quantity);
		return NULL;
	}
	static ProductType *createProductType(const pt& type) {
		if (type == pt::Milk) return new Milk();
		else if (type == pt::Bread) return new Bread();
		else if (type == pt::Apples) return new Apples();
		else if (type == pt::Chicken) return new Chicken();
		else if (type == pt::Eggs) return new Eggs();
		else if (type == pt::Water) return new Water();
		return NULL;
	}
};
