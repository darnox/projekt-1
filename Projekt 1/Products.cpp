#include "stdafx.h"
#include "Products.h"
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

bool ProductType::setPrice(double price) {
	if (price < 0) return 0;
	this->price = price;
	return 1;
}
void ProductType::setBarcode(const string& barcode) {
	this->barcode = barcode;
}
bool ProductType::setQuantity(unsigned int quantity) {
	if (quantity < 0) return 0;
	this->quantity = quantity;
	return 1;
}

ProductType& ProductType::operator+=(const ProductType& right) {
	if (this->name != right.name) throw "Not the same product type!";
	this->quantity += right.quantity;
	return *this;
}
ProductType& ProductType::operator-=(const ProductType& right) {
	if (this->name != right.name) throw "Not the same product type!";
	if (this->quantity >= right.quantity) this->quantity -= right.quantity;
	else this->quantity = 0;
	return *this;
}
ProductType& ProductType::operator= (ProductType& right) {
	if (this != &right) {
		return right;
	}
	return *this;
}
inline ProductType operator+(ProductType left, const ProductType& right){
	left += right;
	return left;
}
inline ProductType operator-(ProductType left, const ProductType& right) {
	left -= right;
	return left;
}
ostream& operator<<(ostream& os, const ProductType& product){
	os << setw(20) << left << product.toString() << setw(10) << left << product.getQuantity()
		<< setw(10) << left << product.getPrice() << setw(10) << left << product.getTotalCost() << setw(14) << left << product.getBarcode();
	return os;
}

string ProductType::toString() const {
	std::map< ProductTypes, const char * > mapProductTypesToString = {
		{ Milk, "Milk" },
		{ Bread, "Bread" },
		{ Water, "Water" },
		{ Eggs, "Eggs" },
		{ Apples, "Apples" },
		{ Chicken, "Chicken" }
	};

	return mapProductTypesToString[name];
}
