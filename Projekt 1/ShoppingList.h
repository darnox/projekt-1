#pragma once
#include "ProductTypeFactory.h"
#include <list>
#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

typedef ProductType::ProductTypes pt;
typedef list<ProductType> prod_list;
typedef prod_list::iterator prod_itr;

class ShoppingList{
public:
	ShoppingList(string name): name(name){}
	~ShoppingList() {
		delete myFactory;
	}

	bool markProductAsChecked(const pt& name);
	bool removeProductFromChecked(const pt& name);
	bool removeProductFromUnchecked(const pt& name);
	void removeAllProductsFormChecked();
	bool addProduct(const pt& name);
	bool addProduct(const pt& name, int quantity);
	bool changeProductPrice(const pt& name, unsigned int new_price);
	bool changeProductQuantity(const pt& name, unsigned int new_quantity);
	bool changeProductBarcode(const pt& name, const string& new_barcode);
	void moveOnList(const pt& name, int position);
	ProductType * mostExpensiveProduct();
	void toString(int mode);

	double getTotalCost() const {
		return total_cost;
	}
	string getName() const {
		return name;
	}

private:
	prod_list unchecked_list;
	prod_list checked_list;

	double total_cost = 0;
	string name;

	ProductTypeFactory* myFactory = new ProductTypeFactory();

	ProductType* findProduct(const pt& name, prod_list * list);
	prod_itr findProductPosition(const pt& name, prod_list * list);
	bool removeProductFormList(ProductType * product, prod_list * list);
	void addProductToList(ProductType * product, prod_list * list);
	void updateTotalCost();
};


