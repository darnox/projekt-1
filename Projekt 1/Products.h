#pragma once
#include <map>
using namespace std;


class ProductType{
public:
	ProductType();
	~ProductType() {}

	enum ProductTypes {
		Milk, Bread, Water, Eggs, Apples, Chicken
	};

	double getPrice() const{
		return price;
	}
	ProductTypes getName() const{
		return name;
	}
	string getBarcode() const {
		return barcode;
	}
	int getQuantity() const {
		return quantity;
	}
	double getTotalCost() const{
		return quantity * price;
	}
	bool setPrice(double price);
	void setBarcode(const string& barcode);
	bool setQuantity(unsigned int quantity);
	string toString() const;

	ProductType& operator+=(const ProductType& right);
	ProductType& operator-=(const ProductType& right);
	ProductType& operator= (ProductType& right);
	friend ostream& operator<<(ostream& os, const ProductType& product);

protected:
	ProductType(ProductTypes name, std::string barcode, double price, int quantity) 
		: name(name), barcode(barcode), price(price), quantity(quantity) {}

private:
	const ProductTypes name;
	string barcode;
	double price;
	unsigned int quantity;	
};

inline ProductType operator+(ProductType left, const ProductType& right);
inline ProductType operator-(ProductType left, const ProductType& right);

class Milk : public ProductType{
public:
	Milk(int quantity = 1) : ProductType(ProductTypes::Milk, "073625891", 3.29, quantity) {}
};

class Bread : public ProductType{
public:
	Bread(int quantity = 1) : ProductType(ProductTypes::Bread, "66518441125", 1.69, quantity) {}
};

class Water : public ProductType{
public:
	Water(int quantity = 1) : ProductType(ProductTypes::Water, "1254785225", 0.99, quantity) {}
};

class Eggs : public ProductType{
public:
	Eggs(int quantity = 10) : ProductType(ProductTypes::Eggs, "7526585223", 0.49, quantity) {}
};

class Apples : public ProductType{
public:
	Apples(int quantity = 1) : ProductType(ProductTypes::Apples, "9626115158", 0.79, quantity) {}
};

class Chicken : public ProductType{
public:
	Chicken(int quantity = 1) : ProductType(ProductTypes::Chicken, "95246328", 14.49, quantity) {}
};

