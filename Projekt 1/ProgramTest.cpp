#include "stdafx.h"
#include "ProgramTest.h"


ProgramTest::ProgramTest(){}


ProgramTest::~ProgramTest(){}

void ProgramTest::runTest1() {
	ShoppingList *list = new ShoppingList("Test");

	list->addProduct(pt::Milk);
	list->addProduct(pt::Bread);
	list->addProduct(pt::Eggs);
	list->addProduct(pt::Water, 7);
	list->addProduct(pt::Apples, 2);
	list->toString(1);

	list->addProduct(pt::Bread, 2);
	list->toString(1);

	list->removeProductFromUnchecked(pt::Eggs);
	list->toString(1);

	list->markProductAsChecked(pt::Milk);
	list->toString(1);

	list->removeProductFromChecked(pt::Milk);
	list->toString(1);

	cout << "MEP: " << list->mostExpensiveProduct()->getPrice() << endl;

	list->addProduct(pt::Chicken, 3);
	list->toString(1);

	cout << "MEP: " << list->mostExpensiveProduct()->getPrice() << endl;

	list->changeProductQuantity(pt::Chicken, 1);
	list->toString(1);

	list->changeProductPrice(pt::Chicken, 20);
	list->toString(1);

	list->moveOnList(pt::Chicken, -2);
	list->toString(1);

	list->moveOnList(pt::Water, 1);
	list->toString(1);

	delete list;

	system("pause");
}

void ProgramTest::runTest2() {
	ShoppingList *list = new ShoppingList("Test");

	list->addProduct(pt::Milk);
	list->addProduct(pt::Bread);
	list->addProduct(pt::Eggs);
	list->addProduct(pt::Water, 7);
	list->addProduct(pt::Apples, 2);

	list->markProductAsChecked(pt::Eggs);
	list->markProductAsChecked(pt::Water);

	list->toString(1);
	list->toString(2);
	list->toString(3);

	delete list;

	system("pause");
}