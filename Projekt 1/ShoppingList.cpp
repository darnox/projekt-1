#include "stdafx.h"
#include "ShoppingList.h"

bool ShoppingList::markProductAsChecked(const pt& name) {
	ProductType *founded = findProduct(name, &unchecked_list);

	if (!founded) return 0;

	total_cost -= founded->getTotalCost();
	addProductToList(founded, &checked_list);
	removeProductFormList(founded, &unchecked_list);

	return 1;
}
bool ShoppingList::removeProductFromChecked(const pt& name) {
	ProductType *founded = findProduct(name, &checked_list);

	if (!founded) return 0;
	removeProductFormList(founded, &checked_list);
	return 1;
}
bool ShoppingList::removeProductFromUnchecked(const pt& name) {
	ProductType *founded = findProduct(name, &unchecked_list);

	if (!founded) return 0;

	total_cost -= founded->getTotalCost();
	removeProductFormList(founded, &unchecked_list);
	return 1;
}
void ShoppingList::removeAllProductsFormChecked() {
	for (std::list<ProductType>::iterator i = checked_list.begin(); i != checked_list.end(); ++i) {
		removeProductFromChecked(i->getName());
	}
}
bool ShoppingList::addProduct(const pt& name) {
	ProductType* new_product = myFactory->createProductType(name);
	if (!new_product) return 0;

	addProductToList(new_product, &unchecked_list);
	total_cost += new_product->getTotalCost();

	delete new_product;
	return 1;
}
bool ShoppingList::addProduct(const pt& name, int quantity) {
	ProductType* new_product = myFactory->createProductType(name, quantity);
	if (!new_product || quantity <= 0) return 0;

	addProductToList(new_product, &unchecked_list);
	total_cost += new_product->getTotalCost();

	delete new_product;
	return 1;
}
bool ShoppingList::changeProductPrice(const pt& name, unsigned int new_price) {
	ProductType *founded = findProduct(name, &unchecked_list);

	if (!founded) return 0;

	if (!founded->setPrice(new_price)) return 0;
	updateTotalCost();

	return 1;
}
bool ShoppingList::changeProductQuantity(const pt& name, unsigned int new_quantity) {
	ProductType *founded = findProduct(name, &unchecked_list);

	if (!founded) return 0;

	if (!founded->setQuantity(new_quantity)) return 0;
	updateTotalCost();

	return 1;
}
bool ShoppingList::changeProductBarcode(const pt& name, const string& new_barcode) {
	ProductType *founded = findProduct(name, &unchecked_list);

	if (!founded) return 0;

	founded->setBarcode(new_barcode);

	return 1;
}
void ShoppingList::toString(int mode) { //mode == 1 - full list , mode == 2 - unchecked list , mode == 3 - checked list 
	cout << setw(20) << left << "List: " << setw(20) << left << name << endl;
	cout << "---------------------------------------------------------------------" << endl;
	cout << setw(4) << left << "|   " << setw(20) << left << "Product Name" << setw(10) << left << "Quantity"
		<< setw(10) << left << "Price" << setw(10) << left << "Cost" << setw(14) << left << "Barcode" << "|" << endl;
	cout << "---------------------------------------------------------------------" << endl;

	if (mode == 2 || mode == 1) {
		cout << setw(44) << left << "| Unchecked list:" << setw(25) << right << "|" << endl;
		for (std::list<ProductType>::iterator i = unchecked_list.begin(); i != unchecked_list.end(); ++i) {
			cout << setw(4) << left << "|" << *i << "|" << endl;
		}
		cout << "---------------------------------------------------------------------" << endl;
		cout << setw(44) << left << "|   Total cost:" << total_cost << setw(20) << right << "|" << endl;
		cout << "---------------------------------------------------------------------" << endl;
	}

	if (mode == 3 || mode == 1) {
		cout << setw(44) << left << "| Checked list:" << setw(25) << right << "|" << endl;
		for (std::list<ProductType>::iterator i = checked_list.begin(); i != checked_list.end(); ++i) {
			cout << setw(4) << left << "|" << *i << "|" << endl;
		}
		if (checked_list.begin() != checked_list.end()) cout << "---------------------------------------------------------------------" << endl;
		else cout << setw(44) << left << "|    - empty" << setw(25) << right << "|" << endl;
	}

	cout << endl << endl << endl;
}
ProductType * ShoppingList::mostExpensiveProduct() {
	ProductType *most_expensive_product = &(*unchecked_list.begin());
	for (list<ProductType>::iterator i = unchecked_list.begin(); i != unchecked_list.end(); ++i) {
		if (i->getPrice() > most_expensive_product->getPrice()) {
			most_expensive_product = &(*i);
		}
	}
	return most_expensive_product;
}
void ShoppingList::moveOnList(const pt& name, int position = 1) {
	prod_itr current = findProductPosition(name, &unchecked_list), new_position;
	if (current == unchecked_list.end()) return;

	if (position > 0) {
		for (new_position = current; position-- && new_position != unchecked_list.end() ; new_position++);
		new_position++;
	} else 
		for (new_position = current; position++ && new_position != unchecked_list.begin(); new_position--);	
	
	unchecked_list.insert(new_position,  *current);

	unchecked_list.erase(current);
}

ProductType* ShoppingList::findProduct(const pt& name, prod_list * list) {
	prod_itr it = findProductPosition(name, list);
	if (it == list->end()) return NULL;
	return &(*it);
}
prod_itr ShoppingList::findProductPosition(const pt& name, prod_list * list) {
	for (prod_itr i = list->begin(); i != list->end(); ++i) {
		if (name == (*i).getName()) return i;
	}
	return list->end();
}
bool ShoppingList::removeProductFormList(ProductType * product, prod_list * list) {
	prod_itr founded = findProductPosition(product->getName(), list);

	if (founded == list->end()) return 0;

	list->erase(founded);

	return 1;
}
void ShoppingList::addProductToList(ProductType * product, prod_list * list) {
	ProductType *founded = findProduct(product->getName(), list);

	if (founded) {
		*founded += *product;
	}
	else {
		list->push_back(*product);
	}
}
void ShoppingList::updateTotalCost() {
	total_cost = 0;
	for (prod_itr i = unchecked_list.begin(); i != unchecked_list.end(); ++i) {
		total_cost += i->getTotalCost();
	}
}